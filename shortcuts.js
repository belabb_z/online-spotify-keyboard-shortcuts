//
//  gitlab.com/belabb_z
//

document.addEventListener('readystatechange', (event) => {
  if (event.target.readyState === 'complete') {
    document.addEventListener('keyup', (keyPressEvent) => {
      switch (keyPressEvent.key) {
        case 'ArrowLeft':
          if (keyPressEvent.ctrlKey) {
            document.querySelector('button.control-button.spoticon-skip-back-16').click();
          }
          break;
        case 'ArrowRight':
          if (keyPressEvent.ctrlKey) {
            document.querySelector('button.control-button.spoticon-skip-forward-16').click();
          }
          break;
        case 'ArrowDown':
          if (keyPressEvent.ctrlKey && keyPressEvent.altKey) {
            document.querySelector('button.spoticon-volume-twowave-16.control-button.volume-bar__icon').click();
          }
          break;
        default:
          break;
      }
    });
  }
});
